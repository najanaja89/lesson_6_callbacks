﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_6_delegates
{
    public class Account
    {
        public string PersonFullName { get; set; }
        public int Sum { get; private set; } = 1;
        public IMessenger Messenger { get; set; }

        public void Add(int sum)
        {
            Sum += sum;
            Messenger.SendMessage("Вы внесли " + sum);
        }

        public void Withdraw(int sum)
        {
            if (sum > Sum)
            {
                Messenger.SendMessage("Сумма больше " + sum);
                return;
            }

            Sum -= sum;
            Messenger.SendMessage("Вы сняли " + sum);
        }

    }
}
