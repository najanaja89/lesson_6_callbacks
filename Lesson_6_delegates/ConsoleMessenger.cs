﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_6_delegates
{
    public class ConsoleMessenger : IMessenger
    {
        public void SendMessage(string message)
        {
            Console.WriteLine(message);
        }
    }
}
